package loancalculator.loancalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
    EditText loanAmount;
    EditText loanTerm;
    EditText yearlyRate;
    TextView monthlyPayment;
    TextView totalPayment;
    TextView totalInterest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loanAmount = (EditText) findViewById(R.id.loanAmount);
        loanTerm = (EditText) findViewById(R.id.loanTerm);
        yearlyRate = (EditText) findViewById(R.id.yearlyInterestRate);
        monthlyPayment = (TextView) findViewById(R.id.monthlyPayment);
        totalPayment = (TextView) findViewById(R.id.totalPayment);
        totalInterest = (TextView) findViewById(R.id.totalInterest);
    }

    public void calculate(View v){
        LoanCalculator calculator = new LoanCalculator(this.getDoubleFromEt(loanAmount),
                this.getIntFromEt(loanTerm), this.getDoubleFromEt(yearlyRate));
        this.setText(Double.toString(calculator.getMonthlyPayment()),
                Double.toString(calculator.getTotalCostOfLoan()), Double.toString(calculator.getTotalInterest()));
    }

    public void clear(View v){
        this.setText("", "", "");
        loanAmount.setText("");
        loanTerm.setText("");
        yearlyRate.setText("");
    }

    public void setText(String monthly, String totalP, String totalI) {
        monthlyPayment.setText(monthly);
        totalPayment.setText(totalP);
        totalInterest.setText(totalI);
    }

    public double getDoubleFromEt(EditText et) {
        String input = et.getText().toString();

        if (input.isEmpty()) {
            et.setText("0");
            return 0;
        }

        return Double.parseDouble(input);
    }

    public int getIntFromEt(EditText et) {
        String input = et.getText().toString();

        if (input.isEmpty()) {
            et.setText("0");
            return 0;
        }

        return Integer.parseInt(input);
    }
}
